**Apithy - Surveys Exam**

---

## Problema

- Se necesitan aplicar encuestas a diferentes usuarios basÃ¡ndonos en su edad
- Actualmente se cuenta con 3 tipos diferentes de preguntas: Rangos, Checks y Radios, cada respuesta tiene un valor especÃ­fico
- La mayorÃ­a de las encuestas se evalÃºan de la siguiente forma (NUM_PREGUNTAS / (SUMATORIA RESPUESTAS)) * 100
- Futuras encuestas podrÃ­an tener un mÃ©todo particular de evaluaciÃ³n , debemos asegurarnos que agregarlas no sea un problema.
- Las encuestas tienen una fecha de expiraciÃ³n de 30 dÃ­as a partir de su asignaciÃ³n
- Actualmente contamos con 3 encuestas pero estimamos llegar a 650 diferentes a fin de aÃ±o
- Las encuestas regularmente son entregadas mediante JSON o XML y tenemos que programar lo necesario para ingresarlas en el sistema
- Necesitamos registrar en la BD los accesos de los usuarios a cada unos de las encuestas asÃ­ como intentos de accesos no vÃ¡lidos a las encuestas y login
- [Detalle de las encuestas](https://docs.google.com/spreadsheets/d/1Ue2PCseKwpWLf-wQP7QFA75F7HIBiPBpsnr0zg7EHKo/edit?usp=sharing)


## Requerimientos

- DiseÃ±ar una BD de forma manual, y dejar el script de creaciÃ³n para su evaluaciÃ³n
- Crear un usuario especÃ­fico en la base de datos con privilegios Ãºnicamente en la base de datos creada, dejar el script como evidencia
- DiseÃ±ar los data sources para las encuestas vÃ­a JSON y XML
- Generar un comando en CLI para cargar las encuestas desde un JSON,XML o un array.
- Solo usuarios autenticados pueden acceder a las encuestas y contestarlas
- Necesitamos un Servicio REST con Endpoints para crear y loguear un usuario, Obtener encuestas, Evaluar las encuestas y Consultar los resultados de las encuestas
- El mÃ©todo del logueo queda a tu elecciÃ³n
- Utilizar Symfony o Laravel para el desarrollo.
- No es obligatorio pero si deseas puedes agregar el frontend usando VueJs,Angular o React, tu creatividad serÃ¡ tomada en cuenta.



##Notas
- Si no logras finalizar el ejercicio no te preocupes de todas formas queremos revisar tus avances
---
